import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import data.Place;
import data.Order;
import data.StateOrder;
import data.StateServer;

public class Server extends AbstractActor {

	ActorRef tableManager;
	ActorRef chefCuisto;
	ActorRef orchestrator;
	private StateServer etat;

	public Server(ActorRef tableManager, ActorRef chefCuisto) {
		this.tableManager = tableManager;
		this.chefCuisto = chefCuisto;
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Order.class, m -> {
			orchestrator = sender();
			System.out.println(self().path().name() + " = je receptionne le client " + m.getClient().path().name());
			System.out.println(self().path().name() + " to " + tableManager.path().name() + " > "
					+ " trouve moi une table pour le client " + m.getClient().path().name());
			tableManager.tell(m, self());
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.EN_COURS, m -> {
			System.out.println(self().path().name() + " to " + m.getOrder().getClient().path().name() + " > "
					+ " vous pouvez vous installer a la table " + m.getTable().getId());
			m.getOrder().getClient().tell(m, self());
			System.out.println(self().path().name() + " to " + chefCuisto.path().name() + " > "
					+ " preparez la commande du client " + m.getOrder().getClient().path().name());
			chefCuisto.tell(m, orchestrator);
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.PRETE, m -> {
			System.out.println(self().path().name() + " = je me rend à la table: " + m.getTable().getId());
			System.out.println(self().path().name() + " to " + m.getOrder().getClient().path().name() + " > "
					+ " Tenez votre commande, bon app' ");
			m.getOrder().getClient().tell(m, self());
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.CONSOMMEE, m -> {
			String customer = m.getOrder().getClient().path().name();
			System.out.println(self().path().name() + " = J' encaisse le client " + customer);
			System.out.println(self().path().name() + " to " + tableManager.path().name() + " > " + " libere la table "
					+ m.getTable().getId());
			tableManager.tell(m.getTable(), self());
		}).build();
	}

	@Override
	public void preStart() {

	}

	public StateServer getEtat() {
		return etat;
	}

	public void setEtat(StateServer etat) {
		this.etat = etat;
	}

}
