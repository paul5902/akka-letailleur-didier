import akka.actor.AbstractActor;
import data.Place;
import data.StateOrder;

import java.util.concurrent.TimeUnit;

public class Cooker extends AbstractActor {

	public Cooker() {
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Place.class, m -> {
			System.out.println(self().path().name() + " = je commence la preparation de la commande de la table "
					+ m.getTable().getId());
			try {
				double time = (Math.random() * (3));
				TimeUnit.SECONDS.sleep((new Double(time)).longValue());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(self().path().name() + " = j'ai terminé la preparation de la commande de la table "
					+ m.getTable().getId());
			m.getOrder().setEtat(StateOrder.PRETE);
			System.out.println(self().path().name() + " to " + sender().path().name()
					+ " > Chef, voici la commande de la table " + m.getTable().getId());
			sender().tell(m, self());
		}).build();
	}

	@Override
	public void preStart() {
	}

}
