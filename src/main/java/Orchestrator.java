import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import data.Place;
import data.Order;
import data.StateOrder;
import data.StateServer;

public class Orchestrator extends AbstractActor {

	ArrayList<ActorRef> servers;
	private int indexServer;

	public Orchestrator(ArrayList<ActorRef> servers) {
		this.servers = servers;
		indexServer = 1;
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Order.class, m -> {
			receptionClient(m);
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.PRETE, m -> {
			servirCommande(m);
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.CONSOMMEE, m -> {
			encaisser(m);
		}).build();
	}

	private void receptionClient(Order commande) {
		ActorRef server = getServer();
		System.out.println(self().path().name() + " = je redirige le client " + sender().path().name()
				+ " vers le server " + server.path().name());
		System.out.println(self().path().name() + " to " + server.path().name() + " > " + commande.getMessage());
		server.tell(commande, self());
	}

	private void servirCommande(Place commande) {
		ActorRef server = getServer();
		System.out.println(self().path().name() + " to " + server.path().name() + " > "
				+ " servez cette commande a la table " + commande.getTable().getId());
		server.tell(commande, self());
	}

	private void encaisser(Place commande) {
		ActorRef server = getServer();
		System.out.println(self().path().name() + " to " + server.path().name() + " > "
				+ " encaissez la commande de la table " + commande.getTable().getId());
		server.tell(commande, self());
	}

	@Override
	public void preStart() {
		System.out.println("Le restaurant ouvre ses portes");
	}

	private ActorRef getServer() {
		ActorRef server = servers.get(indexServer);
		indexServer = Math.floorMod(indexServer + 1, servers.size());
		return server;
	}
}
