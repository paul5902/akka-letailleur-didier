import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainRestaurant {

	private static final int NB_CUISTOS = 4;
	private static final int NB_SERVERS = 2;
	private static final int NB_TABLE = 8;

	public static void main(String[] args) {
		restaurant();
	}

	private static void restaurant() {
		ActorSystem system = ActorSystem.create("restaurant", ConfigFactory.load(("restaurant")));

		ActorRef tableManager = system.actorOf(Props.create(TableManager.class, NB_TABLE), "TableManager");

		ArrayList<ActorRef> cuistos = new ArrayList<ActorRef>();
		for (int i = 0; i < NB_CUISTOS; i++) {
			cuistos.add(system.actorOf(Props.create(Cooker.class), "cuisto" + i));
		}
		ActorRef chefCuisto = system.actorOf(Props.create(ChiefCooker.class, cuistos), "ChefCuisto");

		ArrayList<ActorRef> servers = new ArrayList<ActorRef>();
		for (int i = 0; i < NB_SERVERS; i++) {
			servers.add(system.actorOf(Props.create(Server.class, tableManager, chefCuisto), "server" + i));
		}

		system.actorOf(Props.create(Orchestrator.class, servers), "Orchestrator");

	}
}
