import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import data.Place;
import data.StateOrder;

import java.util.ArrayList;

public class ChiefCooker extends AbstractActor {

	ArrayList<ActorRef> cuistos;
	ActorRef orchestrator;
	int indexCuisto;

	public ChiefCooker(ArrayList<ActorRef> cuistos) {
		this.cuistos = cuistos;
		indexCuisto = 0;
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Place.class, m -> m.getOrder().getEtat() == StateOrder.EN_COURS, m -> {
			receptionCommande(m);
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.PRETE, m -> {
			envoiCommande(m);
		}).build();
	}

	private void receptionCommande(Place commande) {
		if (orchestrator == null) {
			orchestrator = sender();
		}

		System.out.println(
				self().path().name() + " = je prend en charge la commande de la table " + commande.getTable().getId());
		ActorRef cuisto = getFreeCuisto();
		System.out.println(
				self().path().name() + " to " + cuisto.path().name() + " > " + commande.getOrder().getMessage());
		cuisto.tell(commande, self());

	}

	private void envoiCommande(Place commande) {
		System.out.println(self().path().name() + " to " + orchestrator.path().name()
				+ " > un server peut apporter cette commande a la table " + commande.getTable().getId());
		orchestrator.tell(commande, self());
	}

	@Override
	public void preStart() {

	}

	private ActorRef getFreeCuisto() {
		ActorRef cuisto = cuistos.get(indexCuisto);
		indexCuisto = Math.floorMod(indexCuisto + 1, cuistos.size());
		return cuisto;
	}
}
