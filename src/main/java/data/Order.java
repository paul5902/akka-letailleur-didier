package data;

import akka.actor.ActorRef;


import java.io.Serializable;
import java.util.List;



public class Order implements Serializable {

	private static final long serialVersionUID = 1L;
	
	String message;
    ActorRef client;
    List<Meal> plats;
    StateOrder etat;
    
    public Order() {
    }
    
	public Order(String message, ActorRef client, List<Meal> plats, StateOrder etat) {
		super();
		this.message = message;
		this.client = client;
		this.plats = plats;
		this.etat = etat;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ActorRef getClient() {
		return client;
	}
	public void setClient(ActorRef client) {
		this.client = client;
	}
	public List<Meal> getPlats() {
		return plats;
	}
	public void setPlats(List<Meal> plats) {
		this.plats = plats;
	}
	public StateOrder getEtat() {
		return etat;
	}
	public void setEtat(StateOrder etat) {
		this.etat = etat;
	}
    
    
}
