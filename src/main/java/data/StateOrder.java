package data;

import java.io.Serializable;

public enum StateOrder implements Serializable {
	EN_COURS,
	PRETE,
	CONSOMMEE
}
