package data;

import java.io.Serializable;

public class Meal implements Serializable {
    String name;

	public Meal() {
		super();
	}
	
	public Meal(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    
}
