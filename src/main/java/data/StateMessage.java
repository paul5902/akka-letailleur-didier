package data;

import java.io.Serializable;

public enum StateMessage implements Serializable {
    GREET, DONE;
}
