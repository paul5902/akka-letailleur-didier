package data;

import akka.actor.ActorRef;

import java.io.Serializable;

public class Table implements Serializable {
    int id;
    ActorRef client;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ActorRef getClient() {
		return client;
	}
	public void setClient(ActorRef client) {
		this.client = client;
	}
    
    
}
