package data;

import java.io.Serializable;

public enum StateServer implements Serializable {
	FREE,
	BUSY
}
