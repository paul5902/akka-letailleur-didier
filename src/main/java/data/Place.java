package data;

import java.io.Serializable;

public class Place implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Order order;
    Table table;
    
    public Place() {
    	super();
    }

	public Place(Order order, Table table) {
		super();
		this.order = order;
		this.table = table;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}
}
