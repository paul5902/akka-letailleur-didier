import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import akka.actor.AbstractActor;
import data.Place;
import data.Order;
import data.Table;

public class TableManager extends AbstractActor {

    int nbTables;
    List<Table> tables;
    List<Order> fileDAttente;

    public TableManager(int nbTables) {
        this.nbTables = nbTables;
        this.fileDAttente = new ArrayList<Order>();
        this.tables = new ArrayList<>();
        for(int i = 0; i<nbTables; i++){
            Table table = new Table();
            table.setId(i);
            tables.add(table);
        }

    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Order.class, m -> {
                    receptionClient(m);
                })
                .match(Table.class, m -> {
                    libererTable(m);
                })
                .build();
    }

    private void libererTable(Table table) {
    	Table ownTable = getOwnTableById(table.getId());
        System.out.println(self().path().name() + " = je libere la table " + table.getId() );
        if(!fileDAttente.isEmpty()){
        	Order commande = fileDAttente.get(0);
            fileDAttente.remove(commande);
            placerClientALaTable(ownTable, commande);
        }else{
            ownTable.setClient(null);
        }
    }
    
    private Table getOwnTableById(int id){
    	for(Table table: tables){
    		if(table.getId() == id){
    			return table;
    		}
    	}
    	return null;
    }


    private void receptionClient(Order commande) {
        System.out.println(self().path().name() + " = je cherche une place pour le client " + commande.getClient().path().name() );
        Table table = getFreeTable();
        if(table == null){
            System.out.println(self().path().name() + " = je place le client " + commande.getClient().path().name() + " dans la file d attente." );
            fileDAttente.add(commande);
        }else{
            placerClientALaTable(table, commande);
        }
    }

    private void placerClientALaTable(Table table, Order commande) {
        System.out.println(self().path().name() + " = je place le client " + commande.getClient().path().name() + " sur la table " + table.getId() );
        table.setClient(commande.getClient());
        Place attributionPlace = new Place( commande, table);
        System.out.println(self().path().name() + " to " + sender().path().name() + " > " + " le client " + commande.getClient().path().name() + " peut s installer a la table " + table.getId());
        sender().tell(attributionPlace, self());
    }


    private Table getFreeTable() {
        List<Table> freeTables = tables.stream().filter( table ->  table.getClient() == null).collect(Collectors.toList());
        if(freeTables.isEmpty()) return null;
        return freeTables.get(0);
    }


    @Override
    public void preStart() {

    }
}
