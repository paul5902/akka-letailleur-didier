import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import data.Place;
import data.Order;
import data.StateOrder;
import data.StateMessage;
import data.Meal;

public class Client extends AbstractActor {
	ActorSelection orchestrator;

	public Client(ActorSelection orchestrator) {
		this.orchestrator = orchestrator;
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(String.class, m -> {
			System.out.println("client: " + m);
			sender().tell(StateMessage.DONE, self());
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.EN_COURS, m -> {
			System.out.println(self().path().name() + " = je m installe a la table " + m.getTable().getId());
		}).match(Place.class, m -> m.getOrder().getEtat() == StateOrder.PRETE, m -> {
			System.out.println(self().path().name() + " = Je commence mon repas");
			quandVoulezVousPartir();
			m.getOrder().setEtat(StateOrder.CONSOMMEE);
			System.out.println(self().path().name() + " = Je viens de finir mon repas");
			System.out.println(self().path().name() + " to " + orchestrator.pathString() + " > Voici mon paiement");
			orchestrator.tell(m, self());
		}).build();
	}

	private void quandVoulezVousPartir() {
		Scanner reader = new Scanner(System.in);
		System.out.println("Pressez entrer pour partir ");
		reader.nextLine();
		reader.close();
	}

	@Override
	public void preStart() {
		Order creationCommande = new Order();
		String message = "voici ma commande, je suis " + self().path().name();
		creationCommande.setMessage(message);
		creationCommande.setClient(self());
		List<Meal> plats = new ArrayList<>();
		plats.add(new Meal("salade"));
		plats.add(new Meal("steak"));
		plats.add(new Meal("tarte"));
		creationCommande.setPlats(plats);
		creationCommande.setEtat(StateOrder.EN_COURS);

		System.out.println(self().path().name() + " to " + orchestrator.pathString() + " > " + message);
		this.orchestrator.tell(creationCommande, self());
	}
}
