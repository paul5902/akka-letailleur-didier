import java.util.ArrayList;
import java.util.Scanner;

import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class MainClient {

	public static void main(String[] args) {
		client();
	}

	private static void client() {
		ArrayList<ActorRef> clients = new ArrayList<ActorRef>();
		ActorSystem system = ActorSystem.create("client", ConfigFactory.load(("client")));
		String nom = getNameFromClient();
		final ActorSelection orchestrator;
		String url = "akka.tcp://restaurant@127.0.0.1:2552/user/Orchestrator";
		orchestrator = system.actorSelection(url);
		clients.add(system.actorOf(Props.create(Client.class, orchestrator), nom));
	}

	private static String getNameFromClient() {
		Scanner reader = new Scanner(System.in);
		System.out.println("Quelle est votre nom ? ");
		String n = reader.nextLine();
		return n;
	}
}
